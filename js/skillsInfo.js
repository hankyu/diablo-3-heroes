
$(document).ready(function() {
	var heroDetail = JSON.parse(window.localStorage.getItem("heroDetail"));
	var imgURL = 'http://us.media.blizzard.com/d3/icons/skills/42/';
	
	var skills = heroDetail.skills;
	var active = skills.active,
		passive = skills.passive;
	
	for(var i = 0; i < active.length; i++) {
		if(active[i].skill) {
			$('img#active_'+i).attr('src', imgURL+active[i].skill.icon+".png");
			$('b#skill_name_'+i).html(active[i].skill.name);
			if(active[i].rune) {
				$('b#rune_'+i).html(active[i].rune.name);
			} else {
				$('b#rune_'+i).html('');
			}
		} else {
			$('img#active_'+i).attr('src', '');
			$('b#skill_name_'+i).html('');
			$('b#rune_'+i).html('');
		}
	}
	
	for(var i = 0; i < passive.length; i++) {
		if(passive[i].skill) {
			$('img#passive_'+i).attr('src', imgURL+passive[i].skill.icon+".png");
			$('b#skill_name_passive_'+i).html(passive[i].skill.name);
		} else {
			$('img#passive_'+i).attr('src', '');
			$('b#skill_name_passive_'+i).html('');
		}
	}
});
