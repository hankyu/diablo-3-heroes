
$(document).ready(function() {
	
	var hero_list = JSON.parse(window.localStorage.getItem("heroes"));
	//console.log(hero_list);
	var heroes = hero_list.heroes;
	
	for(var i = 0; i < heroes.length; i++) {
		// create li element and append it to listview	
		var hClass = heroes[i].class, 
			hGender = heroes[i].gender;
		var imgURL, imgY;
		
		if(hClass == "barbarian") {
			imgURL = '../img/class/bb.jpg';
			imgY = -235;
		} else if(hClass == "demon-hunter") {
			imgURL = '../img/class/dh.jpg';
			imgY = -230;
		} else if(hClass == "monk") {
			imgURL = '../img/class/mk.jpg';
			imgY = -146;
		} else if(hClass == "witch-doctor") {
			imgURL = '../img/class/wd.jpg';
			imgY = -220;
		} else if(hClass == "wizard") {
			imgURL = '../img/class/wz.jpg';
			imgY = -350;
		}
		
		var heroInfo = "<li id='list-item' style='background: linear-gradient(rgba(255,255,255, 0), rgba(255,255,255,0)), url("+imgURL+") no-repeat 0 "+ imgY +"; background-size:100%;'>" +
							"<div>" +
								"<div>" +
									"<a href='#' style='text-decoration: none;' onclick='heroSelect("+ i +")'>" +
										"<div style='width:100%; height:100%;'>" +
											"<img id='" + hClass+"_"+hGender + "' />" +
											"<h3 id='hero_name' class='fontsforweb_fontid_1056'>" + heroes[i].name + "</h3>" +
											"<b id='hero_level' class='fontsforweb_fontid_1056'>" + heroes[i].level + "</b>" +
											"<b id='hero_paragon' class='fontsforweb_fontid_1056'>" + "(" + heroes[i].paragonLevel + ")" +"</b>" +
										"</div>" +
									"</a>" +
								"</div>" +
							"</div>"+
					   "</li>";
		$("ul#hero-listview").append(heroInfo);
	}
});

function heroSelect(id) {
	var hero_list = JSON.parse(window.localStorage.getItem("heroes"));
	var heroes = hero_list.heroes;
	var host = 'http://us.battle.net';
	// get hero infomations
	var battleName = hero_list.battleTag.split('#')[0];
	var battleCode = hero_list.battleTag.split('#')[1];
	var heroId = heroes[id].id;
	var url = host + "/api/d3/profile/" + battleName + "-" + battleCode + "/hero/" + heroId;
	$.ajax({
      	url: url,
      	dataType: 'jsonp',
      	type: 'get',
      	success: function(data) {
      		//console.log(data);
      		// save data
      		window.localStorage.setItem("heroDetail", JSON.stringify(data));
      		
      		// transition to detail.html
      		$.mobile.changePage("../html/detail.html", {
      			transition: "flow"
      		});
      	}
    });
	
	//window.localStorage.setItem("heroDetail", heroDetail);
}
