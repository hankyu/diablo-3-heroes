

$(document).ready(function() {
	var battletag_name, battletag_code, hero_id;
	var host = 'http://us.battle.net';
	
	/*
	 * popups setting
	 */
	$('div#warning').popup();
	
	/* 
	 * get heroes information
	 */
	$('a#getInfoButton').click(function() {
		
		// check if all fileds are correct
		battletag_name = $('input#bName').val();
		battletag_code = $('input#bCode').val();
		hero_id = $('input#hId').val();
		
		if(battletag_name === '' || battletag_code === '') {
		   	$('div#warning').popup('open');
		} else {
			//var url = host+"/api/d3/profile/"+battletag_name+"-"+battletag_code+"/hero/"+hero_id;
			var url = host+"/api/d3/profile/"+battletag_name+"-"+battletag_code+"/";
			$.ajax({
		      	url: url,
		      	dataType: 'jsonp',
		      	type: 'get',
		      	success: function(data) {
		      		//console.log(data);
		      		// save data
		      		window.localStorage.setItem("heroes", JSON.stringify(data));
		      		
		      		// transition to herolist.html
		      		$.mobile.changePage("../html/herolist.html", {
		      			transition: "flow"
		      		});
		      	}
		    });
		}
	});
});
