
$(document).ready(function() {
	//console.log(JSON.parse(window.localStorage.getItem("heroDetail")));
	var heroDetail = JSON.parse(window.localStorage.getItem("heroDetail"));
	var classBackground = heroDetail.class+"_"+heroDetail.gender;
	var hClass = heroDetail.class;
	var imgURL;
	var imgIconURL = 'http://us.media.blizzard.com/d3/icons/items/large/';
	
	// set up class background
	if(hClass == "barbarian") {
		imgURL = "'../img/class/bb.jpg'";
	} else if(hClass == "demon-hunter") {
		imgURL = "'../img/class/dh.jpg'";
	} else if(hClass == "monk") {
		imgURL = "'../img/class/mk.jpg'";
	} else if(hClass == "witch-doctor") {
		imgURL = "'../img/class/wd.jpg'";
	} else if(hClass == "wizard") {
		imgURL = "'../img/class/wz.jpg'";
	}
	
	$('img#items').attr("id", classBackground);
	$('div#detail-page').css("background", "url("+imgURL+") no-repeat");
	$('div#detail-page').css("background-size", "100% 100%");
	
	// item image setting
	var items = heroDetail.items, 
		bracers = items.bracers,
		feet = items.feet,
		hands = items.hands,
		head = items.head,
		leftFinger = items.leftFinger,
		legs = items.legs,
		mainHand = items.mainHand,
		neck = items.neck,
		offHand = items.offHand,
		rightFinger = items.rightFinger,
		shoulders = items.shoulders,
		torso = items.torso,
		waist = items.waist;
		
	if(bracers) {
		$('img#bracers').attr('src', imgIconURL+bracers.icon+'.png');
		$('img#bracers').css('background-color', bracers.displayColor);
	}
	if(feet) {
		$('img#feet').attr('src', imgIconURL+feet.icon+'.png');
		$('img#feet').css('background-color', feet.displayColor);
	}
	if(hands) {
		$('img#hands').attr('src', imgIconURL+hands.icon+'.png');
		$('img#hands').css('background-color', hands.displayColor);
	}
	if(head) {
		$('img#head').attr('src', imgIconURL+head.icon+'.png');
		$('img#head').css('background-color', head.displayColor);
	}
	if(leftFinger) {
		$('img#leftFinger').attr('src', imgIconURL+leftFinger.icon+'.png');
		$('img#leftFinger').css('background-color', leftFinger.displayColor);
	}
	if(legs) {
		$('img#legs').attr('src', imgIconURL+legs.icon+'.png');
		$('img#legs').css('background-color', legs.displayColor);
	}
	if(mainHand) {
		$('img#mainHand').attr('src', imgIconURL+mainHand.icon+'.png');
		$('img#mainHand').css('background-color', mainHand.displayColor);
	}
	if(neck) {
		$('img#neck').attr('src', imgIconURL+neck.icon+'.png');
		$('img#neck').css('background-color', neck.displayColor);
	}
	if(offHand) {
		$('img#offHand').attr('src', imgIconURL+offHand.icon+'.png');
		$('img#offHand').css('background-color', offHand.displayColor);
	}
	if(rightFinger) {
		$('img#rightFinger').attr('src', imgIconURL+rightFinger.icon+'.png');
		$('img#rightFinger').css('background-color', rightFinger.displayColor);
	}
	if(shoulders) {
		$('img#shoulders').attr('src', imgIconURL+shoulders.icon+'.png');
		$('img#shoulders').css('background-color', shoulders.displayColor);
	}
	if(torso) {
		$('img#torso').attr('src', imgIconURL+torso.icon+'.png');
		$('img#torso').css('background-color', torso.displayColor);
	}
	if(waist) {
		$('img#waist').attr('src', imgIconURL+waist.icon+'.png');
		$('img#waist').css('background-color', waist.displayColor);
	}
});
