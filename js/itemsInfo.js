
$(document).ready(function() {
	var heroDetail = JSON.parse(window.localStorage.getItem("heroDetail"));
	var classBackground = heroDetail.class+"_"+heroDetail.gender;
	var host = 'http://us.battle.net';
	
	// item info popup
	$('div#item-info-popups').popup();
	$('img.equiped').click(function() {

		
		// get item info tooltips
		var itemType = $(this).attr('id');
		var item_data = heroDetail.items[itemType].tooltipParams;
		var url = host + "/api/d3/data/" + item_data;
		$.ajax({
	      	url: url,
	      	dataType: 'jsonp',
	      	type: 'get',
	      	success: function(data) {
	      		//console.log(data);
	      		
	      		// display data
	      		// item name
	      		$('h3#name').html(data.name);
	      		$('p#typeName').html(data.typeName);
	      		$('p#type').html(data.type.id);
	      		if(data.armor) {
	      			$('p#armor').html(data.armor.max);
	      			$('p#dps').html('');
	      			$('span#isArmor').html('Armor');
	      			$('span#isWeapon').html('');
	      			$('span#damage').html('');
	      			$('span#attacksPerSecond').html('');
	      		} else if(data.type.id == 'Amulet' || data.type.id == 'Ring' || data.dps == undefined) {
	      			$('p#armor').html('');
	      			$('p#dps').html('');
	      			$('span#isWeapon').html('');
	      			$('span#damage').html('');
	      			$('span#attacksPerSecond').html('');
	      			$('span#isArmor').html('');
	      		} else {
	      			$('p#armor').html('');
	      			$('p#dps').html(data.dps.max.toFixed(1));
	      			$('span#isArmor').html('');
	      			$('span#isWeapon').html('Damage Per Second');
	      			$('span#damage').html(data.minDamage.min.toFixed(0) + "-" + data.maxDamage.max.toFixed(0) + " Damage");
	      			$('span#attacksPerSecond').html(data.attacksPerSecond.max.toFixed(2) + " Attacks Per Second");
	      		}
	      		
	      		var attributeString = '';
	      		for(var i = 0; i < data.attributes.length; i++) {
	      			attributeString += "<br>" + data.attributes[i] + "</br>";
	      		}
	      		$('p#attributes').html(attributeString);
	      		
	      		var gemString = '';
	      		for(var i = 0; i < data.gems.length; i++) {
	      			gemString += "<br>" + data.gems[i].attributes[0] + "</br>";
	      		}
	      		$('p#gems').html(gemString);
	      		
	      		var setString = '';
	      		if(data.set) {
	      			setString += "<p style='margin-top:1em'>" + data.set.name + "</p>";
	      			for(var i = 0; i < data.set.items.length; i++) {
	      				setString += "<p style='margin-left:1em'>" + data.set.items[i].name + "</p>";	
	      				
	      			}
	      			
	      			for(var i = 0; i < data.set.ranks.length; i++) {
	      				setString += "<p>(" + data.set.ranks[i].required + ") Set</p>";
	      				console.log('attributes', data.set.ranks[i].attributes[0]);
	      				for(var j = 0; j < data.set.ranks[i].attributes.length; j++) {
	      					
	      					setString += "<p style='margin-left:1em'>" + data.set.ranks[i].attributes[j] + "</p>";
	      				}
	      			}
	      			$('p#set').html(setString);
	      		} else {
	      			$('p#set').html('');
	      		}
	      		
	      		if(data.itemLevel) {
	      			$('p#itemLevel').html('Item Level:'+data.itemLevel);	
	      		} else {
	      			$('p#itemLevel').html('');
	      		}
	      		
	      		if(data.requiredLevel) {
	      			$('p#requiredLevel').html('Required Level:'+data.requiredLevel);
	      		} else {
	      			$('p#requiredLevel').html('');
	      		}
	      		
	      		if(data.flavorText) {
	      			$('div#flavorText').html(data.flavorText);
	      		} else {
	      			$('div#flavorText').html('');
	      		}
	      	}
	    });
		
		$('img#item-icon').attr('src', $(this).attr('src'));
		
		// display item info popups
		$('div#item-info-popups').popup('open');
	});

});
