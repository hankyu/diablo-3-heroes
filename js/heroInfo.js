
$(document).ready(function() {
	var heroDetail = JSON.parse(window.localStorage.getItem("heroDetail"));
	var classBackground = heroDetail.class+"_"+heroDetail.gender;
	var host = 'http://us.battle.net';
	
	// display hero info
	console.log(heroDetail);
	var stats = heroDetail.stats;
	$('td#strength').html(stats.strength);
	$('td#dexterity').html(stats.dexterity);
	$('td#intelligence').html(stats.intelligence);
	$('td#vitality').html(stats.vitality);
	$('td#armor').html(stats.armor);
	$('td#damage').html(stats.damage);
	$('td#life').html(stats.life);
	
	//$('td#strength').html(stats.strength);
	var resourceLabel = '', resourceValue = '';
	if(heroDetail.class == 'demon-hunter') {
		// Hatred and Discipline
		resourceLabel = 'Hatred/Discipline:';
		resourceValue = stats.primaryResource + '/' + stats.secondaryResource;
	} else if(heroDetail.class == 'barbarian') {
		// Fury
		resourceLabel = 'Fury:';
		resourceValue = stats.primaryResource;
	} else if(heroDetail.class == 'monk') {
		// Spirit 
		resourceLabel = 'Spirit:';
		resourceValue = stats.primaryResource;
	} else if(heroDetail.class == 'witch-doctor') {
		// Mana
		resourceLabel = 'Mana:';
		resourceValue = stats.primaryResource;
	} else if(heroDetail.class == 'wizard') {
		// Arcane Power
		resourceLabel = 'Arcane Power:';
		resourceValue = stats.primaryResource;
	}
	$('td#cost-label').html(resourceLabel);
	$('td#cost').html(resourceValue);
});
